package ru.tsc.anaumova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.anaumova.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskServiceDTO extends IUserOwnedServiceDTO<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}